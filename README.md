#Rest Services Tool Box

This tool-box attempts to provide a light weight pattern to help teams
 consistently expose and consume REST type web service end-points.

####Some features include:

- Wrapping HttpClient in light weight API to make calling REST endpoints easier
- Consistent client side error handling
- API base objects to carry both status and payload data
- Encourages standard message contracts
- Light weight correlation and identity tracking
- Low barrier to entry.  Useful out-of-the-box and not require custom HTTP Modules, complex configuration, ...
- Simple implementation which makes understanding yet exposes some customizable extension points

###Security Note
There is nothing inherently "secure" about this tool-box.  It is up to the implementation to 
ensure the level of security necessary (transport via SSL, authentication, etc.).  For internal 
services, often core-to-core calls are considered natively secure, or could use SSL.

The tools box is extensible enough that a team can relatively easily address security concerns (e.g. add authentication headers) without impacting the ease-of-use for the application.
##Rest API Design
**The Problem**

Any non-trivial REST services (and service end-point design in general) encounter several design patterns.  Often service authors miss
including critical information service consumers (e.g. application developers) need in order to efficiently and effectively

- Visibility into what went wrong with the request.  Returning a bad status code (>= 400) or null payload often just makes debugging harder
- You want to return more then one message
- How to get a clear picture of what went wrong; especially if the application has made several calls to several services or a page hits multiple load balanced servers before the page errors out. What about when a service calls a service and something went horribly wrong way down the decendent tree.
- Integration with advanced diagnostics tools such as Event Tracing for Windows (ETW) or other syslog type event sinks which use numeric message codes
- Different service authors attempt to address these problems in different ways while others simply return serialized payloads.

**Conclusion**

Rest Services Tool Box does not address all these issues (specifically distributed event tracking), but it does provide tools that might help expose helpful information when debugging and/or reacting to adverse responses.

###HTTP Status Codes & Verbs
HTTP Status Codes are designed to work with the HTTP Protocol but in a RESTful service environment, 
the status codes are often overloaded to reflect business logic as well. It is important successful REST service  to use consistent HTTPStatus Codes.  
Often there are discrepancies and interpretations so a team should agree on what status codes should be used when. The only wrong answer is not to be consistent.

When designing a REST API a suggested pattern might be:


Action    | VERB   |   Status Code(s)         | Request Body        | Url                | Response Payload
--------- | ------ | ------------------------ | ------------------- | ------------------ | --------------------- |
Create a resource|POST|201 - Created,409 - Conflict|Serialized (text) body|"/"|Id of created object
Update existing resource|PUT|200 - OK,404 - Not Found|Serialized (text) body|"/{resourceId}"|(none)
Get specific resource|GET|200 - OK, 404 - Not Found | (none) | "/?{query string parameters}" | Serialized (text) body
Get (find) similar resources|GET|200 - OK, 204 - No Content|(none)|"/?{query string parameters}"|Array/List of Serialized object (text)
Remove a resource | DELETE| 200 - OK, 404 - Not Found |(none)| "/{resourceId}"|(none)

1. These are just suggested patterns.  The actual API should be well documented in a place that is accessible to all developer's wishing access to the API.
1. When using the APIReponseBase class(es) all response payloads will always have a "Status" object attached.

Standard HTTP Status Codes can be found here:

[W3.Org RFC2616](http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html)

[Wikipedia](http://en.wikipedia.org/wiki/List_of_HTTP_status_codes)

###Response Contracts
Response contracts returned from the service.  Many times API writers only return business objects.  This prevents the service from communicating 
important information back to the caller.  For example, a business rule may fail throwing an "500 Internal Server Error".  In this case we have no idea of the
cause of the failure and on high-speed services, it will be difficult to trace through logs to find the specific entry related to this error.

**Key:** An effective API must expose business objects and allow visibility into the service by the caller.  
**Key:** An effective API must provide a consistent structure that the caller can depend on.

The REST Toolbox contains a class `ApiResponseBase` that returns service specific status back to the caller.
Here is an example:

	GET-Response Example Success
	{
		"Status" : {
			"StatusCode": 200,
		}
		"Payload" : {
			"street1": "123 main street",
			"street2": null,
			"city": "happy place",
			"state: "HW",
			"postalCode": "12345-6789"
		}
	}

	POST-Response Example Error Message
	{
		"Status" : {
			"StatusCode": 404,
			"Message": "Cannot connect downstream server on http://localhost:3838/api/demosevice1",
			"ErrorDetails": "<stack trace goes here>"
		}
	}

###Server Side Code

Response messages should be simple POCOs so they can be consumed by non .Net sources (JavaScript/AJAX, JAVA, etc.). 
Exactly how much functionality is built into the response message contracts will be determined by the use-cases faced 
by the team.

Disclaimer:  These examples are for illustrative purposes only.  Build your own best-practices objects.  Just be consistent

	Using preferred pattern:

	public class CustomerAddressResponse : ApiResponseBase<CustomerAddress>
	{
		//message contains two fields:
		//  ApiStatus Status;
		//  CustomerAddress Payload;

		//example but often constructors are often application specific
		public CustomerAddressResponse(CustomerAddress address, int statusCode, string message, params object[] args):base(address) //initialize
		{
			this.Status.StatusCode = statusCode,
			this.Message = string.Format(message,args);
		}
	}

	Alternative patterns:

		public class CustomerAddressResponse : ApiResponseBase //automatically adds 'Status' field
		{
			public CustomerAddress Address {get;set;} //use this pattern if you do not want to use the nominal data field 'Payload'
		}
		
		public class CustomerAddressResponse : ApiResponseBase  //automatically adds 'Status' field
		{
			
			public string street1 {get;set;}
			public string street2 {get;set;}
			public string city {get;set;}
			public string state {get;set;}
			public string postalCode {get;set;}
		}

Sometimes you need to return only a status and/or informational message (e.g. you API might allow DELETE of non-existing resources):

##Rest Client
**Quick Start -- Code By Example**

	(1) using(var client = new RestClient("http://webservices.example.domain/api"))
	{
	(2)	var session = client.GET("services?{0}",serviceId);
	(3)	if (session.IsOk)
		{
	(4)		myObject = session.Request.Payload<myType>();
		}
	}

What this does:

1. Create a REST client and set the end-point for our service.  For advanced scenarios, you can control default formatters (XML, JSON, etc.), time outs, exception handling, etc.  This client can handle multiple calls so only dispose of it when done making REST calls.
2. Call the service.  There's some pretty cool stuff going on in the background but from an application's perspective it doesn't matter.  Notice you can provide traditional string.format parameters as you build out the URL.  Something little to make your life easier.
3. Check to see if the call succeeded.  The session object contains much information to help you react to unexpected situations.  For this example we are just checking that everything worked as expected.
4. Now get the body of the returned message and convert it into something we can use 


**Rest Client API Summary**

    RestSession GET(string url, params[] args)
    RestSession PUT(object body, string url, params object[] args)
    RestSession PUT(string body, string url, params object[] args)
    RestSession POST(object body, string url, params object[] args)
    RestSession POST(string body, string url, params object[] args)
    RestSession DELETE(object body, string url, params object[] args)
    RestSession DELETE(string body, string url, params object[] args)
    RestSession DELETE(string url, params object[] args)
    RestSession UPLOAD(Dictionary<string, byte[]> attachments, string url, params object[] args)


##Extensibility
The rest client is extensible in a few key areas.  In order to keep it relatively simple, this client doesn't attempt to be extensible to cover all conceivable options.  Each team/project might create a specialized client that better interfaces to their own 
Here is where you can easily inherit from the RestClient and adjust it's functionality:

- Add/remove serializers (MediaTypeFormatters).  For example replacing JSon.Net with ServiceStack.Text
- OnBeforeSend handler
- OnSuccess handler
- OnError handler
- OnCompleted handler - for logging, etc.
- Add customer headers ("x-correlationId")


