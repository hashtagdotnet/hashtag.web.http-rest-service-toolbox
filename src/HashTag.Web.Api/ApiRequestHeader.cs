﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HashTag.Web.Api
{
    public class ApiRequestHeader
    {
        public ApiRequestHeader()
        {
            CallerReference = Guid.NewGuid().ToString();
        }

        public ApiRequestHeader(string callerReference)
        {
            CallerReference = callerReference;
        }

        /// <summary>
        /// Any short tracking text caller wants to provide to service.  Service implementations should return this value in responses.  NOTE:  Service may truncate too long of reference
        /// </summary>
        public string CallerReference { get; set; }
    }
}
