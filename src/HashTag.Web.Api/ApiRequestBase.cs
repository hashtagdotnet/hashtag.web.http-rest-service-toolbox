﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HashTag.Web.Api
{
    public class ApiRequestBase
    {
        public ApiRequestHeader ApiHeader { get; set; }

        public ApiRequestBase()
        {
            ApiHeader = new ApiRequestHeader();         
        }
    }
}
