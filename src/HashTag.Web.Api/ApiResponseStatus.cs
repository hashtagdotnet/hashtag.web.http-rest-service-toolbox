﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

namespace HashTag.Web.Api
{
    /// <summary>
    /// Defines a standard 'header' package that can be returned from an API end-point message.  Used in 
    /// conjunction with the primary returned object.  Can used in inheritance or composition models 
    /// </summary>
    public class ApiResponseStatus
    {
        /// <summary>
        /// The value of this status from the API perspective. Often parallel to API returned StatusCode in HTTP Header but sometimes it will diverge
        /// </summary>
        public HttpStatusCode StatusCode { get; set; }

        /// <summary>
        /// System wide identifier (e.g. AP10001, HR92922, ERR.10) that identifies this message and/or message state (success, error).  
        /// Often used in routing, response strategies, and especially localization as a key to resource files
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string MessageCode { get; set; }

        /// <summary>
        /// Api Dependent.  Text caller might display to user but provided by system
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string DisplayMessage { get; set; }

        /// <summary>
        /// Api Dependent.  Detailed system level information (error messages, reference numbers, etc) but not generally displayed to user.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string SystemMessage { get; set; }

        /// <summary>
        /// Place holder where a .Net exception information  is being returned to caller.  Often in core-to-core calls where 
        /// application developers might need service error details to help troubleeshoot service
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public LogException Exception { get; set; }

        /// <summary>
        /// Generic text provided by caller's request that identifies this call.  A request-reposne correlation id.  Often used when
        /// caller makes several async calls to service and caller needs to track each response
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string CallerReference { get; set; }

        /// <summary>
        /// Return TRUE if StatusCode &lt; 400
        /// </summary>
        public bool IsOk
        {
            get
            {
                return ((int)StatusCode < 400 && Exception == null);
            }
        }
        /// <summary>
        /// Well formatted, human readable, representation of this status
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendLine("{0,-16}{1}", "Status:", StatusCode.ToString());
            sb.AppendLine("{0,-16}{1}", "IsOK:", IsOk.ToString());

            if (string.IsNullOrWhiteSpace(MessageCode) == false)
            {
                sb.AppendLine("{0,-16}{1}", "Message Code:", MessageCode);
            }
            if (string.IsNullOrWhiteSpace(DisplayMessage) == false)
            {
                sb.AppendLine("{0,-16}{1}", "Display Message:", DisplayMessage.Replace(Environment.NewLine,Environment.NewLine+new string(' ',19)));
            }
            if (string.IsNullOrWhiteSpace(SystemMessage) == false)
            {
                sb.AppendLine("{0,-16}{1}", "System Message:", SystemMessage.Replace(Environment.NewLine, Environment.NewLine + new string(' ', 19)));
            }
            if (Exception != null)
            {
                sb.AppendLine("{0,-16}{1}", "Exception:", Exception.ToString().Replace(Environment.NewLine, Environment.NewLine + new string(' ', 19)));
            }
            if (string.IsNullOrWhiteSpace(CallerReference) == false)
            {
                sb.AppendLine("{0,-16}{1}", "Reference:", CallerReference.Replace(Environment.NewLine, Environment.NewLine + new string(' ', 19)));
            }
            return sb.ToString();
        }
    }
}
