﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HashTag.Web.Api
{
    public class ApiPageRequest
    {
        public int PageSize { get; set; }
        public bool RequestTotalRows { get; set; }
        public int PageNumber { get; set; }
        public string[] SortColumns { get; set; }
    }
}
