﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HashTag.Web.Api
{    
    public class ApiLink
    {
        [JsonProperty(PropertyName = "title")]
        public string Title { get; set; }

        [JsonProperty(PropertyName="href")]
        public string Location { get; set; }

        [JsonProperty(PropertyName = "rel")]
        public string Relation { get; set; }

        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }
    }
}
