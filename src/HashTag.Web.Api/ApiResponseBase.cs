﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HashTag.Web.Api
{
    public class ApiResponseBase
    {
        public ApiResponseBase()
        {
            Status = new ApiResponseStatus();
        }

        public ApiResponseStatus Status { get; set; }
    }
}
