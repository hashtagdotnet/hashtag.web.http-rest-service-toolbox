﻿using HashTag.Web.Api;
using HashTag.Web.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;

namespace RestClientTests
{
    public class GetValueListResponse : ApiResponseBase
    {
        public List<string> IdList { get; set; }
    }
    public class TestRequest
    {
        public int Id { get; set; }
        public int IntProp { get; set; }
        public DateTime DateTimeProp { get; set; }
        public string StrProp { get; set; }
        public decimal DecProp { get; set; }
        public Guid? NullableGuidProp { get; set; }
    }

    public class TestResponse : ApiResponseBase
    {
        public int Id { get; set; }
        public int IntProp { get; set; }
        public DateTime DateTimeProp { get; set; }
        public string StrProp { get; set; }
        public decimal DecProp { get; set; }
        public Guid? NullableGuidProp { get; set; }
    }

    public class ValuesController : ApiController
    {

        // GET api/values 
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5 
        public HttpResponseMessage Get(int id)
        {
            var msg = new ApiResponseBase();
            switch (id)
            {
                case 99500:
                    msg.Status.StatusCode = HttpStatusCode.InternalServerError;
                    msg.Status.DisplayMessage = "Could not find id: " + id.ToString();
                    msg.Status.SystemMessage= "Something went horribly wrong";
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, msg);
                case 99404:
                    msg.Status.StatusCode = HttpStatusCode.NotFound;
                    msg.Status.DisplayMessage = "Could not find id: " + id.ToString();
                    return Request.CreateResponse(HttpStatusCode.NotFound, msg);
                case 99204:
                    msg.Status.StatusCode = HttpStatusCode.NoContent;
                    msg.Status.DisplayMessage = "Could not find id: " + id.ToString();
                    return Request.CreateResponse(HttpStatusCode.NoContent, msg);
                default:
                    var retMessage = new HttpResponseMessage(HttpStatusCode.OK);
                    retMessage.Content = new StringContent("value");
                    return retMessage;
            }
        }

        // POST api/values 
        public HttpResponseMessage Post(TestRequest value)
        {
            var msg = new TestResponse();
            switch (value.Id)
            {
                case 79200:
                    msg = mapper(value);
                    msg.Status.StatusCode = HttpStatusCode.OK;
                    return Request.CreateResponse(msg.Status.StatusCode, msg);
                default:
                    msg.Status.StatusCode = HttpStatusCode.NotFound;
                    return Request.CreateResponse(HttpStatusCode.NotFound, msg);
            }
        }

        // PUT api/values/5 
        public HttpResponseMessage Put(int id, TestRequest value)
        {
            var msg = new TestResponse();
            switch (id)
            {
                case 89200:
                    msg = mapper(value);
                    msg.Status.StatusCode = HttpStatusCode.OK;
                    return Request.CreateResponse(msg.Status.StatusCode, msg);
                default:
                    msg.Status.StatusCode = HttpStatusCode.NotFound;
                    return Request.CreateResponse(HttpStatusCode.NotFound, msg);
            }
        }

        private TestResponse mapper(TestRequest value)
        {
            var retVal = new TestResponse()
            {
                DateTimeProp = value.DateTimeProp,
                DecProp = value.DecProp,
                Id = value.Id,
                IntProp = value.IntProp,
                NullableGuidProp = value.NullableGuidProp,
                StrProp = value.StrProp
            };
            return retVal;
        }

        // DELETE api/values/5 
        public HttpResponseMessage Delete(int id, TestRequest value)
        {
            var msg = new TestResponse();
            switch (id)
            {
                case 69200:
                    msg.Status.StatusCode = HttpStatusCode.OK;
                    return Request.CreateResponse(msg.Status.StatusCode, msg);
                case 69250:
                    msg = mapper(value);
                    msg.Status.StatusCode = HttpStatusCode.OK;                    
                    return Request.CreateResponse(msg.Status.StatusCode, msg);
                case 69404:
                    msg.Status.StatusCode = HttpStatusCode.NotFound;
                    return Request.CreateResponse(msg.Status.StatusCode, msg);
                default:
                    msg.Status.StatusCode = HttpStatusCode.NotFound;
                    return Request.CreateResponse(HttpStatusCode.NotFound, msg);
            }
        }
    }
}
