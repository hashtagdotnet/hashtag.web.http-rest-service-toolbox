﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Owin;
using System.Web.Http;
using Microsoft.Owin.Hosting;
namespace RestClientTests
{
    [TestClass]
   public class TestUtils
    {
        static IDisposable _client;
        [AssemblyInitialize]
        public static void InitializeAssembly(TestContext ctx)
        {
            
            ctx.Properties["baseAddress"] = BaseAddress;
            
            // Start OWIN host 
            _client= Microsoft.Owin.Hosting.WebApp.Start<TestUtils>(url: BaseAddress);
            

        }
        [AssemblyCleanup]
        public static void CleanUpAssembly()
        {
            if (_client != null)
            {
                _client.Dispose();
            }
        }
        public static string BaseAddress
        {
            get
            {
                return "http://localhost:59999/";
            }
        }
        // This code configures Web API. The Startup class is specified as a type
        // parameter in the WebApp.Start method.
        public void Configuration(IAppBuilder appBuilder)
        {
            // Configure Web API for self-host. 
            HttpConfiguration config = new HttpConfiguration();
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            appBuilder.UseWebApi(config);
        } 
    }
}
