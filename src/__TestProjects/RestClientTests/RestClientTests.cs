﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net.Http;
using System.Net;
using HashTag.Web.Http;
using System.Collections.Generic;
using HashTag.Web.Api;

namespace RestClientTests
{

    [TestClass]
    public class RestClientTests
    {
        private TestContext testContextInstance;
        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        [TestMethod]
        public void CheckSelfHostIsAlive()
        {
            HttpClient client = new HttpClient();
            var response = client.GetAsync(baseAddress + "api/values").Result;
            Assert.AreEqual<HttpStatusCode>(HttpStatusCode.OK, response.StatusCode);
            TestContext.WriteLine("{0}", response);
        }

        [TestMethod]
        public void ApiNewClient_NoPath_NoMediaType_NoException()
        {
            var client = RestApi.NewClient();
            Assert.IsTrue(string.IsNullOrWhiteSpace(client.CurrentConfiguration.BaseUrl));
            Assert.AreEqual<string>("application/json",client.CurrentConfiguration.DefaultBodyType);
        }

        [TestMethod]
        public void ApiNewClient_Path_NoMediaType_NoException()
        {
            var baseUrl = "http://testurl";

            var client = RestApi.NewClient(serviceBaseUrl: baseUrl);
            Assert.AreEqual<string>(baseUrl, client.CurrentConfiguration.BaseUrl);
            Assert.AreEqual<string>("application/json", client.CurrentConfiguration.DefaultBodyType);
        }

        [TestMethod]
        public void ApiNewClient_Path_MediaType_NoException()
        {
            var baseUrl = "http://testurl";
            var mediaType = "application/testmedia";

            var client = RestApi.NewClient(serviceBaseUrl: baseUrl, defaultBodyFormat: mediaType);
            Assert.AreEqual<string>(baseUrl, client.CurrentConfiguration.BaseUrl);
            Assert.AreEqual<string>(mediaType, client.CurrentConfiguration.DefaultBodyType);
        }

        [TestMethod]
        public void ClientGet_ExpectOKWithData()
        {
            using (var client = RestApi.NewClient(baseAddress))
            {
                var session = client.GET("/api/values/99200");

                Assert.AreEqual<bool>(true, session.IsOk);
                Assert.AreEqual<bool>(true, session.IsCallOk);
                Assert.AreEqual<HttpStatusCode>(HttpStatusCode.OK, session.StatusCode);

                //var results = session.Response.DeserializeBody<string>();
                Assert.AreEqual<string>("value", session.Response.Body);

                TestContext.WriteLine(session.ToString());
            }
        }

        [TestMethod]
        public void ClientGet_UrlWithParams_ExpectOKWithData()
        {
            using (var client = RestApi.NewClient(baseAddress))
            {
                var id = 99200;
                var session = client.GET("/api/values/{0}", id);

                Assert.AreEqual<bool>(true, session.IsOk);
                Assert.AreEqual<bool>(true, session.IsCallOk);
                Assert.AreEqual<HttpStatusCode>(HttpStatusCode.OK, session.StatusCode);

                //var results = session.Response.DeserializeBody<string>();
                Assert.AreEqual<string>("value", session.Response.Body);

                TestContext.WriteLine(session.ToString());
            }
        }

        [TestMethod]
        public void ClientGet_NoServiceData_ExpectNoContent()
        {
            using (var client = RestApi.NewClient(baseAddress))
            {
                var session = client.GET("/api/values/99204");

                Assert.AreEqual<bool>(true, session.IsOk);
                Assert.AreEqual<bool>(true, session.IsCallOk);
                Assert.AreEqual<HttpStatusCode>(HttpStatusCode.NoContent, session.StatusCode);

                var results = session.Response.DeserializeBody<ApiResponseBase>();


                TestContext.WriteLine(session.ToString());
            }
        }

        [TestMethod]
        public void ClientGet_NoServiceData_ExpectNotFound()
        {
            using (var client = RestApi.NewClient(baseAddress))
            {
                var session = client.GET("/api/values/99404");

                Assert.AreEqual<bool>(false, session.IsOk);
                Assert.AreEqual<bool>(true, session.IsCallOk);
                Assert.AreEqual<HttpStatusCode>(HttpStatusCode.NotFound, session.StatusCode);

                var results = session.Response.DeserializeBody<ApiResponseBase>();


                TestContext.WriteLine("{0}", session.ToString());

            }
        }

        [TestMethod]
        public void ClientGet_NoServiceData_ExpectInternalServerError()
        {
            using (var client = RestApi.NewClient(baseAddress))
            {
                var session = client.GET("/api/values/99500");

                Assert.AreEqual<bool>(false, session.IsOk);
                Assert.AreEqual<bool>(true, session.IsCallOk);
                Assert.AreEqual<HttpStatusCode>(HttpStatusCode.InternalServerError, session.StatusCode);

                var results = session.Response.DeserializeBody<ApiResponseBase>();

                TestContext.WriteLine("{0}", session.ToString());
            }
        }

        [TestMethod]
        public void ClientGet_MissingUrl_ExpectNotFound()
        {
            using (var client = RestApi.NewClient(baseAddress))
            {
                var session = client.GET("/api/v1/values/99500");

                Assert.AreEqual<bool>(false, session.IsOk);
                Assert.AreEqual<bool>(true, session.IsCallOk);
                Assert.AreEqual<HttpStatusCode>(HttpStatusCode.NotFound, session.StatusCode);

                var results = session.Response.DeserializeBody<ApiResponseBase>();

                TestContext.WriteLine("{0}", session.ToString());
            }
        }

        [TestMethod]
        public void ClientPut_SendObject_ExpectOK()
        {
            using (var client = RestApi.NewClient(baseAddress))
            {
                var id = 89200;
                var msg = new TestRequest()
                {
                    DateTimeProp = DateTime.Now,
                    DecProp = 3.9M,
                    Id = id,
                    IntProp = 4847,
                    NullableGuidProp = null,
                    StrProp = "this is a test"
                };
                var session = client.PUT(msg, "/api/values/{0}", id);

                Assert.AreEqual<bool>(true, session.IsOk);
                Assert.AreEqual<bool>(true, session.IsCallOk);
                Assert.AreEqual<HttpStatusCode>(HttpStatusCode.OK, session.StatusCode);

                var results = session.Response.DeserializeBody<TestResponse>();
                Assert.AreEqual<DateTime>(msg.DateTimeProp, results.DateTimeProp);
                Assert.AreEqual<Decimal>(msg.DecProp, results.DecProp);
                Assert.AreEqual<int>(msg.IntProp, results.IntProp);
                Assert.AreEqual<string>(msg.StrProp, results.StrProp);

                TestContext.WriteLine("{0}", session.ToString());
            }
        }

        [TestMethod]
        public void ClientPost_SendObject_ExpectOK()
        {
            using (var client = RestApi.NewClient(baseAddress))
            {
                var id = 79200;
                var msg = new TestRequest()
                {
                    DateTimeProp = DateTime.Now,
                    DecProp = 3.9M,
                    Id = id,
                    IntProp = 4847,
                    NullableGuidProp = null,
                    StrProp = "this is a test"
                };
                var session = client.POST(msg, "/api/values");

                Assert.AreEqual<bool>(true, session.IsOk);
                Assert.AreEqual<bool>(true, session.IsCallOk);
                Assert.AreEqual<HttpStatusCode>(HttpStatusCode.OK, session.StatusCode);

                var results = session.Response.DeserializeBody<TestResponse>();
                Assert.AreEqual<DateTime>(msg.DateTimeProp, results.DateTimeProp);
                Assert.AreEqual<Decimal>(msg.DecProp, results.DecProp);
                Assert.AreEqual<int>(msg.IntProp, results.IntProp);
                Assert.AreEqual<string>(msg.StrProp, results.StrProp);

                TestContext.WriteLine("{0}", session.ToString());
            }
        }

        [TestMethod]
        public void ClientDelete_DeleteExistingId_ExpectOK()
        {
            using (var client = RestApi.NewClient(baseAddress))
            {
                var id = 69200;
             
                var session = client.DELETE("/api/values/{0}",id);

                Assert.AreEqual<bool>(true, session.IsOk);
                Assert.AreEqual<bool>(true, session.IsCallOk);
                Assert.AreEqual<HttpStatusCode>(HttpStatusCode.OK, session.StatusCode);
            }
        }
        [TestMethod]
        public void ClientDelete_DeleteMissingId_ExpectNotFound()
        {
            using (var client = RestApi.NewClient(baseAddress))
            {
                var id = 69404;

                var session = client.DELETE("/api/values/{0}", id);

                Assert.AreEqual<bool>(false, session.IsOk);
                Assert.AreEqual<bool>(true, session.IsCallOk);
                Assert.AreEqual<HttpStatusCode>(HttpStatusCode.NotFound, session.StatusCode);
            }
        }
        [TestMethod]
        public void ClientDelete_DeletePassingObjectReturningObject_ExpectOK()
        {
            using (var client = RestApi.NewClient(baseAddress))
            {
                var id = 69250;
                var msg = new TestRequest()
                {
                    DateTimeProp = DateTime.Now,
                    DecProp = 3.9M,
                    Id = id,
                    IntProp = 4847,
                    NullableGuidProp = null,
                    StrProp = "this is a test"
                };
                var session = client.DELETE(msg,"/api/values/{0}", id);

                Assert.AreEqual<bool>(true, session.IsOk);
                Assert.AreEqual<bool>(true, session.IsCallOk);
                Assert.AreEqual<HttpStatusCode>(HttpStatusCode.OK, session.StatusCode);

                var results = session.Response.DeserializeBody<TestResponse>();
                Assert.AreEqual<DateTime>(msg.DateTimeProp, results.DateTimeProp);
                Assert.AreEqual<Decimal>(msg.DecProp, results.DecProp);
                Assert.AreEqual<int>(msg.IntProp, results.IntProp);
                Assert.AreEqual<string>(msg.StrProp, results.StrProp);
            }
        }
        private string baseAddress
        {
            get
            {
                return TestUtils.BaseAddress;
            }
        }
    }
}
