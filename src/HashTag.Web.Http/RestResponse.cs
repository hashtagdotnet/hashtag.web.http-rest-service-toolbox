﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Http;
using System.Net;
using System.IO;
using HashTag.Collections;

namespace HashTag.Web.Http
{
    /// <summary>
    /// Contains details of a single result of a REST service call
    /// </summary>
    public class RestResponse:IDisposable
    {
        /// <summary>
        /// True if there are no rest client errors and the http web service did not return an error code (>=400)
        /// </summary>
        public bool IsOk
        {
            get
            {
                return ((int)StatusCode) < 400 && CallException == null && string.IsNullOrWhiteSpace(ErrorMessage);
            }
        }

        /// <summary>
        /// True if response has a body received from client
        /// </summary>
        public bool HasBody
        {
            get
            {
                return string.IsNullOrWhiteSpace(Body) == false;
            }
        }

        /// <summary>
        /// Any error message the client wants to set during its execution of the call.
        /// </summary>
        public string ErrorMessage { get; set; }

        HttpResponseMessage _responseMessage = null;
        /// <summary>
        /// Actual message at the HttpLevel (rarely directly accessed by client applications)
        /// </summary>
        public HttpResponseMessage Web { get; set; }

        /// <summary>
        /// Binary representation of the body of the response
        /// </summary>
        public byte[] Payload { get; set; }

        /// <summary>
        /// String representation of the body of the response
        /// </summary>
        public string Body { get; set; }

        /// <summary>
        /// Standard .Net StatusCode as received from HTTP
        /// </summary>
        public HttpStatusCode StatusCode { get; set; }

        /// <summary>
        /// Status text as received from HTTP
        /// </summary>
        public string StatusText { get; set; }

        /// <summary>
        /// Standard MIME type (e.g. application/json) for the body
        /// </summary>
        public string ContentType { get; set; }

        /// <summary>
        /// Total body length in bytes
        /// </summary>
        public long ContentLength { get; set; }

     
        /// <summary>
        /// DateTime when last byte was received from response stream
        /// </summary>
        public DateTime ReceivedDateTime { get; set; }

        /// <summary>
        /// List of key/value headers
        /// </summary>
        public PropertyBag Headers { get; set; }

        /// <summary>
        /// Returned value of the LocationHeader on response (esp. if there is a 201 or 302). Or null if Location is not in header's collectionConvenience method
        /// </summary>
        public string Location
        {
            get
            {
                if (Headers.Contains("Location") == true)
                {
                    return Headers["Location"].Value;
                }
                return null;
            }
        }

        /// <summary>
        /// Internal exception (usually not WebException) client wants to assign to this session.
        /// </summary>
        public LogException ExecuteException { get; set; }

        /// <summary>
        /// Default constructor.
        /// </summary>
        public RestResponse()
        {
            Headers = new PropertyBag();
        }

        /// <summary>
        /// Constructor.  Initialize class with content of exception and any embedded response stream data
        /// </summary>
        /// <param name="exception">Exception encountered when calling service</param>
        public RestResponse(HttpRequestException exception)
            : this()
        {
            CallException = new LogException(exception);
        }

        /// <summary>
        /// Constructor.  Initialize class with embedded response stream data
        /// </summary>
        /// <param name="response">Response as received from service call</param>
        public RestResponse(HttpResponseMessage response)
            : this()
        {
            this.LoadHttpResponseMessage(response); //also forces updating of response properties
        }


        /// <summary>
        /// .Net WebException, if any, that occured during processing service response
        /// </summary>
        public LogException CallException { get; set; }

        /// <summary>
        /// Convert the body into a strongly typed object use configurated deserializer for the approprite body format (JSon,Xml)
        /// </summary>
        /// <typeparam name="T">Type of object to create</typeparam>
        /// <returns>Hydrated body</returns>
        public T DeserializeBody<T>() where T:class
        {
            return RestApi.DeserializeBody<T>(this);
        }

        /// <summary>
        /// Deserialize body using a custom deserializer
        /// </summary>
        /// <typeparam name="T">Type of object to create</typeparam>
        /// <param name="deserializer">Function that will do the deserialization, override the configured one</param>
        /// <returns>Hydrated body</returns>
        public T DeserializeBody<T>(Func<string, T> deserializer)
        {
            return deserializer.Invoke(this.Body);
        }

        /// <summary>
        /// Deserialize bytes using a custom deserializer
        /// </summary>
        /// <typeparam name="T">Type of object to create</typeparam>
        /// <param name="deserializer">Function that will do the deserialization, override the configured one</param>
        /// <returns>Hydrated body</returns>
        public T DeserializeBody<T>(Func<byte[], T> deserializer)
        {
            return deserializer.Invoke(this.Payload);
        }

        /// <summary>
        /// Converts response into a well formatted string suitable for display and logging
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append((int)this.StatusCode);
            sb.AppendLine(" " + this.StatusText);
            foreach (var header in this.Headers)
            {
                sb.AppendLine("{0}: {1}", header.Key, header.Value);
            }
            sb.AppendLine();
            if (string.IsNullOrWhiteSpace(this.ContentType) == false)
            {
                if (this.ContentType.Contains("xml"))
                {
                    string xml = this.Body.ToPrettyXml();
                    xml = xml.Replace("<?xml version=\"1.0\" encoding=\"utf-16\" standalone=\"yes\"?>\r\n", "");
                    sb.AppendLine(xml);
                }
                else
                {
                    sb.AppendLine(this.Body.Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>", ""));
                }
            }
            if (CallException != null)
            {
                sb.AppendLine("");
                sb.AppendLine(CallException.ToString());
            }

            return sb.ToString();
        }

        /// <summary>
        /// Load all the details of the .Net framework response into the RestResponse
        /// </summary>
        /// <param name="msg"></param>
        internal void LoadHttpResponseMessage(HttpResponseMessage msg)
        {
            StatusCode = msg.StatusCode;
            StatusText = msg.ReasonPhrase;
            if (msg != null && msg.Content != null)
            {
                ContentLength = msg.Content.Headers.ContentLength.HasValue ? msg.Content.Headers.ContentLength.Value : -1;
                if (ContentLength > 0 && msg.Content.Headers.ContentType != null)
                {
                    ContentType = msg.Content.Headers.ContentType.MediaType;
                }

                string hdrString = string.Format("{0}", msg.Headers);
                var headerParts = hdrString.Split(new string[] { "\r\n" }, StringSplitOptions.None);
                for (int x = 0; x < headerParts.Length; x++)        //we have to do this because HttpResponseMessage doesn't have a simple kvp
                {
                    var header = headerParts[x].Trim();             //Pragma: no-cache
                    if (string.IsNullOrWhiteSpace(header) == true) continue;
                    var splitPos = header.IndexOf(':');             //Pragma:^no-cache
                    var key = header.Substring(0, splitPos);        //Pragma
                    var value = header.Substring(splitPos + 1);       //no-cache

                    Headers.Add(key.Trim(), value.Trim());
                }
            }
            
            
            if (msg.Content != null)
            {
                if (ContentLength > 0)
                {
                    this.Payload = new byte[ContentLength];
                    using (var ms = new MemoryStream(this.Payload))
                    {
                        msg.Content.CopyToAsync(ms).Wait();
                        ms.Flush();
                    }
                }
                var webResponseString = msg.Content.ReadAsStringAsync().Result;
                this.ReceivedDateTime = DateTime.Now;
                this.Body = webResponseString;
                this.Web = msg;
            }

        }

        public void Dispose(bool isDisposing)
        {
            if (isDisposing == true)
            {
                if (Web != null)
                {
                    Web.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
        }
        ~RestResponse()
        {
            Dispose(false);
        }
    }
}
