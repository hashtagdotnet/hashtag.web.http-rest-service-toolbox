﻿using System;
using util = HashTag;

namespace HashTag.Web.Http
{
    /// <summary>
    /// Entry point into REST client and misc helper methods
    /// </summary>
    public class RestApi
    {
        /// <summary>
        /// Serialization methods.  For now just pass through to Core.Serialize family.  May be modified if extensions are needed
        /// </summary>
        public class Serialize
        {
            public class To
            {
                public static string Json(object obj)
                {
                    return util.Serialize.To.Json(obj);
                }

                public static byte[] Bytes(object obj)
                {
                    return util.Serialize.To.Binary(obj);                    
                }

                public static string Xml(object obj)
                {
                    return util.Serialize.To.Xml(obj);
                }
            }
            public class From
            {
                public static T Json<T>(string serializedJson) where T:class
                {
                    return util.Serialize.From.Json<T>(serializedJson);
                }
                public static T Bytes<T>(byte[] serializedObject)
                {
                    return util.Serialize.From.Binary<T>(serializedObject);
                }

                public static T Xml<T>(string serializedXml)
                {
                    return util.Serialize.From.Xml<T>(serializedXml);
                }
                public static T Xml<T>(byte[] serializedXml)
                {
                    return util.Serialize.From.Xml<T>(serializedXml);
                }
            }
        }

        /// <summary>
        /// Creates a new HTTP Rest client that can be used to issue calls against a rest service end-point
        /// </summary>
        /// <param name="serviceBaseUrl">Url prefix that will be prepended to every other client VERB(x) call. May have trailing slash but will be discareded if suffix begins with a slash.  (eg. http://myservice:8493/api/lotterpicker)</param>
        /// <param name="defaultBodyFormat">This will set the ACCEPT header on each call</param>
        /// <returns>A new client with parameterized settings</returns>
        public static RestClient NewClient(string serviceBaseUrl=null, string defaultBodyFormat = "application/json")
        {
            var config = new RestConfig(){BaseUrl = serviceBaseUrl, DefaultBodyType = defaultBodyFormat};
            
            
            var client = new RestClient(config);
            return client;            
        }

        /// <summary>
        /// Helper method to deserialize a response based on that response's content type.  If body is null then return default(T)
        /// </summary>
        /// <typeparam name="T">Type to serialize into</typeparam>
        /// <param name="response">Valid response with body content</param>
        /// <returns>Object from response</returns>
        public static T DeserializeBody<T>(RestResponse response) where T:class
        {
            if (response.HasBody ==false)
            {
                return default(T);
            }
            
            if (response != null && response.Web != null && response.Web.Content != null && response.Web.Content.Headers != null && string.IsNullOrWhiteSpace(response.Web.Content.Headers.ContentType.MediaType)==false)
            {
                switch (response.Web.Content.Headers.ContentType.MediaType)
                {
                    case "application/json":
                        return RestApi.Serialize.From.Json<T>(response.Body);
                    case "application/xml":
                        return RestApi.Serialize.From.Xml<T>(response.Body);
                    default:
                        throw new NotImplementedException(string.Format("Unable to automatically deserialize body to: {0}", response.Web.Content.Headers.ContentType.MediaType));
                }
            }
            throw new ArgumentException("Unable to find response content to deserialize.  RestResponse.Body may be null or emtpy");
        }
    }
}
