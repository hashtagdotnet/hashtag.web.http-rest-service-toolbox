﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Http;
using System.IO;
using System.Net;
using System.Net.Http.Headers;
using System.Configuration;

namespace HashTag.Web.Http
{

    /// <summary>
    /// Opens a channel to a REST based service.  Can be used to access other kinds of HTTP end-points but may need some trail and error.
    /// </summary>
    public class RestClient : HttpClient,IDisposable
    {
        private RestConfig activeConfig;

        /// <summary>
        /// Default Constructor.  Uses hard-coded configfuration
        /// </summary>
        public RestClient():base()
        {
            activeConfig = new RestConfig();
        }

        /// <summary>
        /// Constructor.  Sets configuration BaseUrl property.  Use '[connectionStringName]'  to read url from .config &lt;connectionStrings&gt; section
        /// </summary>
        /// <param name="urlStem">Part of url that will be prepended to every request.  Allows for shorter relative urls in actual VERB calls</param>
        /// <param name="args">Any arguments needed to supply to <paramref name="urlStem"/></param> e.g 'http://{0}/myservice/api' &lt;= 'devserver:2324'
        public RestClient(string urlStem, params object[] args)
            : this()
        {
            urlStem = resolveUrl(urlStem);
            activeConfig.BaseUrl = string.Format(urlStem, args);
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="config">Settings that control how client connects to service end-point</param>
        public RestClient(RestConfig config)
        {
            activeConfig = config;
        }

        /// <summary>
        /// Settings that conrol how client connects to service end-point.  Modify thesse settings
        /// to customize behaviors and/or connect to internal events
        /// </summary>
        public RestConfig CurrentConfiguration
        {
            get
            {
                return activeConfig;
            }
            set
            {
                activeConfig = value;
            }
        }

        /// <summary>
        /// Low level doing all the heavy lifting of connecting to service, transmitting message, and handling response.  Uses default configuration within client
        /// NOTE:  Do not normally call this method directly.  Use one of the VERB overloads.
        /// </summary>
        /// <param name="session">Session with hydrated request object</param>
        /// <returns>Session with response (and/or exceptions) populated</returns>
        /// <remarks>NOTE:  NOT tested with multi-part or attachment inbound messages</remarks>
        public RestSession EXECUTE(RestSession session)
        {
            return EXECUTE(session, CurrentConfiguration);
        }
        /// <summary>
        /// Low level doing all the heavy lifting of connecting to service, transmitting message, and handling response.
        /// NOTE:  Do not normally call this method directly.  Use one of the VERB overloads.
        /// </summary>
        /// <param name="session">Session with hydrated request object</param>
        /// <param name="sessionConfig">Active configuration to use for this execution instance</param>
        /// <returns>Session with response (and/or exceptions) populated</returns>
        /// <remarks>NOTE:  NOT tested with multi-part or attachment inbound messages</remarks>
        public RestSession EXECUTE(RestSession session, RestConfig sessionConfig)
        {
            session.Response = new RestResponse();
            try
            {
              
                using (HttpClient client = sessionConfig.Client)
                {

                    var channel = client.SendAsync(session.Request.Web);
                    if (channel.Wait((int)sessionConfig.CallTimeOutMs) == false)
                    {
                        session.Response.ErrorMessage = string.Format("Call to {0} timed out after {1}ms", session.Request.Web.RequestUri.ToString(), client.Timeout.Milliseconds);
                        session.Response.StatusCode = HttpStatusCode.GatewayTimeout;
                        return session;
                    }
                    
                    session.Response.LoadHttpResponseMessage(channel.Result); //load result from HTTP call into session.Resposne
                    session.Request.LoadHttpRequestMessage(session.Response.Web.RequestMessage); //load request details (esp. headers injected by WebAPI)
                    
                    if (sessionConfig.OnSessionComplete != null)
                    {
                        sessionConfig.OnSessionComplete.Invoke(session);
                    }
                }
            }
            catch (Exception ex)
            {
                session.ExecuteException = new LogException(ex);
                session.Response.StatusCode = HttpStatusCode.InternalServerError;
                session.Response.ErrorMessage = session.ExecuteException.ToString();

                if (sessionConfig.OnSessionError != null)
                {
                    sessionConfig.OnSessionError(session, ex);
                }                
            }
            return session;

        }

        /// <summary>
        /// Execute a GET request on the client. May contain standard string.format arguments (e.g. "/myService/{id}"
        /// </summary>
        /// <param name="url">Full or relative url of service method.  May contain standard string.format arguments (e.g. "/myService/{id}" )</param>
        /// <param name="args">Any arguments to supply to <paramref name="url"/>.  Optional</param>
        /// <returns>Hydrated result of call</returns>
        public RestSession GET(string url, params object[] args)
        {
            var session = new RestSession();
            try
            {
                var webRequest = activeConfig.NewRequest(HttpMethod.Get, string.Format(url, args));
                session.Request = new RestRequest(webRequest);

                session.Request.LoadHttpRequestMessage(session.Request.Web);
                return EXECUTE(session);
            }
            catch (Exception ex)
            {
                session.ExecuteException = new LogException(ex);
                if (CurrentConfiguration.OnSessionError != null)
                {
                    CurrentConfiguration.OnSessionError(session, ex);
                } 
                return session;
            }
        }

        /// <summary>
        /// Sends an object to service using PUT
        /// </summary>
        /// <param name="body">Serializable hydrated object to send</param>
        /// <param name="url">Full or relative url of service method.  May contain standard string.format arguments (e.g. "/myService/{id}" )</param>
        /// <param name="args">Any arguments to supply to <paramref name="url"/>.  Optional</param>
        /// <returns>Hydrated result of call.  Some services might return a body of some kind</returns>
        public RestSession PUT(object body, string url, params object[] args)
        {
            var session = new RestSession();
            try
            {
                session.Request = new RestRequest();
                session.Request.Body = activeConfig.Serialize(body);

                var webRequest = activeConfig.NewRequest(HttpMethod.Put, string.Format(url, args));
                webRequest.Content = new StringContent(session.Request.Body);
                webRequest.Content.Headers.ContentType = new MediaTypeHeaderValue(CurrentConfiguration.DefaultBodyType);
                session.Request.Web = webRequest;

                session.Request.LoadHttpRequestMessage(session.Request.Web);
                
                return EXECUTE(session);
            }
            catch (Exception ex)
            {
                session.ExecuteException = new LogException(ex);
                if (CurrentConfiguration.OnSessionError != null)
                {
                    CurrentConfiguration.OnSessionError(session, ex);
                } 
                return session;
            }
        }

        /// <summary>
        /// Sends an string to service using PUT
        /// </summary>
        /// <param name="body">String of data (e.g JSon or XML)</param>
        /// <param name="url">Full or relative url of service method.  May contain standard string.format arguments (e.g. "/myService/{id}" )</param>
        /// <param name="args">Any arguments to supply to <paramref name="url"/>.  Optional</param>
        /// <returns>Hydrated result of call.  Some services might return a body of somekind</returns>
        public RestSession PUT(string body, string url, params object[] args)
        {
            var session = new RestSession();
            try
            {
                session.Request = new RestRequest();
                session.Request.Body = body;

                var webRequest = activeConfig.NewRequest(HttpMethod.Put, string.Format(url, args));
                webRequest.Content = new StringContent(body);
                webRequest.Content.Headers.ContentType = new MediaTypeHeaderValue(CurrentConfiguration.DefaultBodyType);
                session.Request.Web = webRequest;

                session.Request.LoadHttpRequestMessage(session.Request.Web);
                return EXECUTE(session);
            }
            catch (Exception ex)
            {
                session.ExecuteException = new LogException(ex);
                if (CurrentConfiguration.OnSessionError != null)
                {
                    CurrentConfiguration.OnSessionError(session, ex);
                } 
                return session;
            }
        }


        /// <summary>
        /// Sends an object to service using PUT
        /// </summary>
        /// <param name="body">Serializable hydrated object to send</param>
        /// <param name="url">Full or relative url of service method.  May contain standard string.format arguments (e.g. "/myService/{id}" )</param>
        /// <param name="args">Any arguments to supply to <paramref name="url"/>.  Optional</param>
        /// <returns>Hydrated result of call.  Some services might return a body of some kind</returns>
        public RestSession POST(object body, string url, params object[] args)
        {
            var session = new RestSession();
            try
            {
                session.Request = new RestRequest();
                session.Request.Body = activeConfig.Serialize(body);

                var webRequest = activeConfig.NewRequest(HttpMethod.Post, string.Format(url, args));
                webRequest.Content = new StringContent(session.Request.Body);
                webRequest.Content.Headers.ContentType = new MediaTypeHeaderValue(CurrentConfiguration.DefaultBodyType);
                session.Request.Web = webRequest;

                session.Request.LoadHttpRequestMessage(session.Request.Web);
                return EXECUTE(session);
            }
            catch (Exception ex)
            {
                session.ExecuteException = new LogException(ex);
                if (CurrentConfiguration.OnSessionError != null)
                {
                    CurrentConfiguration.OnSessionError(session, ex);
                } 
                return session;
            }
        }

        /// <summary>
        /// Sends an string to service using POST
        /// </summary>
        /// <param name="body">String of data (e.g JSon or XML)</param>
        /// <param name="url">Full or relative url of service method.  May contain standard string.format arguments (e.g. "/myService/{id}" )</param>
        /// <param name="args">Any arguments to supply to <paramref name="url"/>.  Optional</param>
        /// <returns>Hydrated result of call.  Some services might return a body of somekind</returns>
        public RestSession POST(string body, string url, params object[] args)
        {
            var session = new RestSession();
            try
            {
                session.Request = new RestRequest();
                session.Request.Body = body;

                var webRequest = activeConfig.NewRequest(HttpMethod.Post, string.Format(url, args));
                webRequest.Content = new StringContent(body);
                webRequest.Content.Headers.ContentType = new MediaTypeHeaderValue(CurrentConfiguration.DefaultBodyType);
                session.Request.Web = webRequest;

                session.Request.LoadHttpRequestMessage(session.Request.Web);
                return EXECUTE(session);
            }
            catch (Exception ex)
            {
                session.ExecuteException = new LogException(ex);
                if (CurrentConfiguration.OnSessionError != null)
                {
                    CurrentConfiguration.OnSessionError(session, ex);
                } 
                return session;
            }
        }

        /// <summary>
        /// Sends an object to service using DELETE (this is not a normal use-case)
        /// </summary>
        /// <param name="body">Serializable hydrated object to send</param>
        /// <param name="url">Full or relative url of service method.  May contain standard string.format arguments (e.g. "/myService/{id}" )</param>
        /// <param name="args">Any arguments to supply to <paramref name="url"/>.  Optional</param>
        /// <returns>Hydrated result of call.  Some services might return a body of some kind</returns>        
        public RestSession DELETE(object body, string url, params object[] args)
        {
            var session = new RestSession();
            try
            {
                session.Request = new RestRequest();
                session.Request.Body = activeConfig.Serialize(body);
                
                var webRequest = activeConfig.NewRequest(HttpMethod.Delete, string.Format(url, args));
                webRequest.Content = new StringContent(session.Request.Body);
                webRequest.Content.Headers.ContentType = new MediaTypeHeaderValue(CurrentConfiguration.DefaultBodyType);
                session.Request.Web = webRequest;

                session.Request.LoadHttpRequestMessage(session.Request.Web);
                return EXECUTE(session);
            }
            catch (Exception ex)
            {
                session.ExecuteException = new LogException(ex);
                if (CurrentConfiguration.OnSessionError != null)
                {
                    CurrentConfiguration.OnSessionError(session, ex);
                } 
                return session;
            }
        }

        /// <summary>
        /// Sends an string to service using DELETE (this is not a common use-case)
        /// </summary>
        /// <param name="body">String of data (e.g JSon or XML)</param>
        /// <param name="url">Full or relative url of service method.  May contain standard string.format arguments (e.g. "/myService/{id}" )</param>
        /// <param name="args">Any arguments to supply to <paramref name="url"/>.  Optional</param>
        /// <returns>Hydrated result of call.  Some services might return a body of somekind</returns>
        public RestSession DELETE(string body, string url, params object[] args)
        {
            var session = new RestSession();
            try
            {
                session.Request = new RestRequest();
                session.Request.Body = body;

                var webRequest = activeConfig.NewRequest(HttpMethod.Delete, string.Format(url, args));
                webRequest.Content = new StringContent(body);
                webRequest.Content.Headers.ContentType = new MediaTypeHeaderValue(CurrentConfiguration.DefaultBodyType);
                session.Request.Web = webRequest;

                session.Request.LoadHttpRequestMessage(session.Request.Web);
                return EXECUTE(session);
            }
            catch (Exception ex)
            {
                session.ExecuteException = new LogException(ex);
                if (CurrentConfiguration.OnSessionError != null)
                {
                    CurrentConfiguration.OnSessionError(session, ex);
                } 
                return session;
            }
        }

        /// <summary>
        /// Sends calls DELETE on a service 
        /// </summary>
        /// <param name="body">String of data (e.g JSon or XML)</param>
        /// <param name="url">Full or relative url of service method.  May contain standard string.format arguments (e.g. "/myService/{id}" )</param>
        /// <param name="args">Any arguments to supply to <paramref name="url"/>.  Optional</param>
        /// <returns>Hydrated result of call.  Some services might return a body of somekind</returns>
        public RestSession DELETE(string url, params object[] args)
        {
            var session = new RestSession();
            try
            {
                session.Request = new RestRequest();

                var webRequest = activeConfig.NewRequest(HttpMethod.Delete, string.Format(url, args));
                session.Request.Web = webRequest;

                session.Request.LoadHttpRequestMessage(session.Request.Web);
                
                return EXECUTE(session);
            }
            catch (Exception ex)
            {
                session.ExecuteException = new LogException(ex);
                if (CurrentConfiguration.OnSessionError != null)
                {
                    CurrentConfiguration.OnSessionError(session, ex);
                } 
                return session;
            }
        }

        /// <summary>
        /// Sends files (actually named binary blobs) to a service using mulitpart/form-date
        /// </summary>
        /// <param name="attachments">List of filename,bits to post to service.  There may be several up to a common sense limit</param>
        /// <param name="url">Full or relative url of service method.  May contain standard string.format arguments (e.g. "/myService/{id}" )</param>
        /// <param name="args">Any arguments to supply to <paramref name="url"/>.  Optional</param>
        /// <returns>Result of the upload.  Attachments are sent as a block so there are not indivual return status</returns>
        public RestSession UPLOAD(Dictionary<string, byte[]> attachments, string url, params object[] args)
        {
            var session = new RestSession();
            try
            {
                session.Request = new RestRequest();

                var webRequest = activeConfig.NewRequest(HttpMethod.Delete, string.Format(url, args));
                var content = new MultipartFormDataContent();
                foreach (var attachment in attachments)
                {
                    content.Add(new ByteArrayContent(attachment.Value), "file", attachment.Key);
                }
                session.Request.Web.Content = content;
                session.Request.Web = webRequest;

                session.Request.LoadHttpRequestMessage(session.Request.Web);
                return EXECUTE(session);
            }
            catch (Exception ex)
            {
                session.ExecuteException = new LogException(ex);
                if (CurrentConfiguration.OnSessionError != null)
                {
                    CurrentConfiguration.OnSessionError(session, ex);
                } 
                return session;
            }
        }

        private string resolveUrl(string url)
        {
            var resolvedUrl = url.Trim();
            if (url.StartsWith("[") == true)
            {
                var result = ConfigurationManager.ConnectionStrings[url]; //look for "[myservice]" key
                if (result != null)
                {
                    resolvedUrl = result.ConnectionString;
                }
                else
                {
                    url = url.Replace("[", "").Replace("]", "");
                    result = ConfigurationManager.ConnectionStrings[url]; //look for "myservice" key
                    if (result != null)
                    {
                        resolvedUrl = result.ConnectionString;
                    }
                    else
                    {
                        throw ExceptionFactory.New<ConfigurationErrorsException>("Unable to resolve url '{0}' using .config file", url);
                    }
                }
            }
            return resolvedUrl;
        }

        //var fileContent = new ByteArrayContent(attachment.Value); //this code is an alternative if UPLOAD doesn't work.  There isn't an UPLOAD use case yet
        //fileContent.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
        //{
        //    FileName = attachment.Key                        
        //};
        //content.Add(content);

        public void Dispose(bool isDisposing)
        {
            if (isDisposing)
            {
                if (activeConfig != null)
                {
                    activeConfig.Dispose();
                }
            }
            GC.SuppressFinalize(this);
        }
        public void Dispose()
        {
            Dispose(true);
        }
        ~RestClient()
        {
            Dispose(false);
        }
    }
}
