﻿using HashTag.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;

namespace HashTag.Web.Http
{
    /// <summary>
    /// Contains details of single HTTP request from REST client
    /// </summary>
    [Serializable]
    public class RestRequest:IDisposable
    {
        /// <summary>
        /// Default constructor
        /// </summary>
        public RestRequest()
        {
            this.SentDateTime = DateTime.Now;
            Headers = new PropertyBag();
        }

        /// <summary>
        /// Constructor.  Initialize this class from HTTP WebRequest
        /// </summary>
        /// <param name="request">HTTP WebRequest</param>
        public RestRequest(HttpRequestMessage request)
            : this()
        {
            LoadHttpRequestMessage(request);
        }

        /// <summary>
        /// Full URL including transport (e.g. http://myservices/account/1234)
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// MIME content type that is sent with request
        /// </summary>
        public string ContentType { get; set; }

        /// <summary>
        /// DateTime when content was submitted to Web
        /// </summary>
        public DateTime SentDateTime { get; set; }

        /// <summary>
        /// Length (in bytes) of headers as submitted to Uri
        /// </summary>
        public long HeaderLength { get; set; }

        /// <summary>
        /// Length (in bytes) of body as submitted to Uri
        /// </summary>
        public long ContentLength { get; set; }

        /// <summary>
        /// Http Verb (GET,POST,DELETE,PUT) used for this request
        /// </summary>
        public string Method { get; set; }

        /// <summary>
        /// List of headers as supplied sent in request
        /// </summary>
        public PropertyBag Headers { get; set; }
        private byte[] _body;

        /// <summary>
        /// Bytes of body using ASCII encoding
        /// </summary>
        public byte[] Payload
        {
            get
            {
                return _body;
            }
            set
            {
                _body = value;
                if (_body != null && _body.Length != 0)
                {
                    ASCIIEncoding encoding = new ASCIIEncoding();
                    Body = encoding.GetString(_body);
                }
            }
        }


        /// <summary>
        /// Textual representation of body sent to service
        /// </summary>
        public string Body { get; set; }

        /// <summary>
        /// Actual message at the HttpLevel (rarely directly accessed by client applications)
        /// </summary>
        public HttpRequestMessage Web { get; set; }
        /// <summary>
        /// Formatted string of request.  Not suitable for serialization
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("{0} {1}", this.Method, this.Address);
            foreach (var header in this.Headers)
            {
                sb.AppendLine("{0}: {1}", header.Key, header.Value);
            }
            if (Body != null && Body.Length > 0)
            {
                sb.AppendLine();
                sb.AppendLine(this.Body);
            }
            return sb.ToString();
        }

        internal void LoadHttpRequestMessage(HttpRequestMessage msg)
        {
            this.HeaderLength = 0;

            this.Method = msg.Method.ToString();
            this.Address = msg.RequestUri.AbsoluteUri;
            this.Web = msg;

            this.Address = msg.RequestUri.AbsoluteUri;

            if (msg.Content != null)
            {
                this.ContentType = msg.Content.Headers.ContentType.MediaType;
                this.ContentLength = msg.Content.Headers.ContentLength.HasValue ? msg.Content.Headers.ContentLength.Value : -1;
            }

            string hdrString = string.Format("{0}", msg.Headers);
            var headerParts = hdrString.Split(new string[] { "\r\n" }, StringSplitOptions.None);
            for (int x = 0; x < headerParts.Length; x++)        //we have to do this because HttpResponseMessage doesn't have a simple kvp
            {
                var header = headerParts[x].Trim();             //Pragma: no-cache
                if (string.IsNullOrWhiteSpace(header) == true) continue;
                var splitPos = header.IndexOf(':');             //Pragma:^no-cache
                var key = header.Substring(0, splitPos);        //Pragma
                var value = header.Substring(splitPos + 1);       //no-cache

                Headers.Add(key.Trim(), value.Trim());
            }
        }

        public void Dispose(bool isDisposing)
        {
            if (isDisposing)
            {
                if (Web != null)
                {
                    Web.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
        ~RestRequest()
        {
            Dispose(false);
        }
    }
}
