﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

namespace HashTag.Web.Http
{
    /// <summary>
    /// All details about a single request/response
    /// </summary>
    public class RestSession:IDisposable
    {
        /// <summary>
        /// Default constructor
        /// </summary>
        public RestSession() { }

        /// <summary>
        /// Returns True if successfully contacted service and received a reply
        /// </summary>
        public bool IsCallOk
        {
            get
            {
                return ExecuteException == null;
            }
        }

        /// <summary>
        /// Returns true if service call to web service was successful *AND* the returned StatusCode from the service is &lt; 400
        /// </summary>
        public bool IsOk
        {
            get
            {
                return IsCallOk && Response != null && Response.IsOk;
            }
        }

       
        /// <summary>
        /// Convenience Wrapper.  Deserializes response body into a strong object
        /// </summary>
        /// <typeparam name="T">Type to convert response body into</typeparam>
        /// <returns>Deserialized body or some kind of serialization exception on error</returns>
        public T DeserializeResponse<T>() where T:class
        {
            return RestApi.DeserializeBody<T>(this.Response);
        }

        /// <summary>
        /// Convenience Wrapper.  Get's status code of session's response or default of InternalServerError (500)
        /// </summary>
        public HttpStatusCode StatusCode
        {
            get
            {
                if (this.Response  != null)
                {
                    return Response.StatusCode;
                }

                return HttpStatusCode.InternalServerError;
            }
        }

        /// <summary>
        /// Convenience Wrapper.  Returns body of *Response* or null if response is not populated
        /// </summary>
        public string Body
        {
            get
            {
                if (this.Response != null)
                {
                    return Response.Body;
                }
                return null;
            }
        }

        /// <summary>
        /// Time it took for request/response including reading all response bytes from stream
        /// </summary>
        public virtual TimeSpan ElapsedTime
        {

            get
            {
                if (Request != null && Response != null)
                {
                    return Response.ReceivedDateTime - Request.SentDateTime;
                }
                if (Request != null && Response == null)
                {
                    return DateTime.Now - Request.SentDateTime;
                }

                return new TimeSpan(0);

            }
            set
            {
                // for serialization compatibility
            }
        }

        /// <summary>
        /// Internal exception that happened when building/sending/receiving HttpRequest
        /// </summary>
        public LogException ExecuteException { get; set; }

        /// <summary>
        /// Request part of this session.  May be null if request has not been set
        /// </summary>
        public RestRequest Request { get; set; }

        /// <summary>
        /// Response part of this session.  May be null if response has not yet been received
        /// </summary>
        public RestResponse Response { get; set; }

        /// <summary>
        /// Formats session into a well defined string
        /// </summary>
        /// <returns>Session as a display compatible string</returns>
        public override string ToString()
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("{0,14}:{1,14}", "Call Ok", IsCallOk.ToString());
                sb.AppendLine("{0,14}:{1,14}", "Message Ok", IsOk.ToString());
                sb.AppendLine("----- Request {0}", new string('-', 55));
                sb.AppendLine(Request.ToString());
                sb.AppendLine();
                sb.AppendLine("----- Response {0}", new string('-', 55));
                sb.Append(Response.ToString());
                sb.AppendLine();
                sb.AppendLine("----- Statistics {0}", new string('-', 55));
                sb.AppendLine("{0,14}:{1,14:HH:mm:ss.fff}", "Sent Time", this.Request.SentDateTime);
                if (this.Response != null)
                {
                    sb.AppendLine("{0,14}:{1,14:HH:mm:ss.fff}", "Received Time", this.Response.ReceivedDateTime);
                }
                sb.AppendLine("{0,14}:{1,10:0}", "ElapsedMs", (this.Response.ReceivedDateTime - this.Request.SentDateTime).TotalMilliseconds);
                if (this.ExecuteException != null)
                {
                    sb.AppendLine(this.ExecuteException.ToString());
                }
                return sb.ToString();
            }
            catch (Exception ex)
            {
                string s = ex.Expand();
                return s+Environment.NewLine+Serialize.To.Json(this);
            }
        }

        public void Dispose(bool isDisposing)
        {
            if (isDisposing == true)
            {
                if (Request != null)
                {
                    Request.Dispose();
                }
                if (Response != null)
                {
                    Response.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
        }
        ~RestSession()
        {
            Dispose(false);
        }
    }
}
